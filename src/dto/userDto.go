package dto

import "github.com/peak-fit/krithick_j/src/models"

type UserIn struct {
	UserName string `json:"user_name"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

type AuthOut struct {
	UserDetails models.User `json:"user_details"`
	AuthToken string `json:"auth_token"`
}

type EditUserIn struct {
	UserName       string  `json:"user_name"`
	Gender         string  `json:"gender"`
	Age            int     `json:"age"`
	Height         float64 `json:"height"`
	Weight         float64 `json:"weight"`
	DisplayProfile string  `json:"display_profile"`
	PhysicalLevel  string  `json:"physical_level"`
	Email          string  `json:"email"`
}
