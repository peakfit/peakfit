package repositories

import (
	"errors"

	"github.com/peak-fit/krithick_j/configs"
	"github.com/peak-fit/krithick_j/src/dto"
	"github.com/peak-fit/krithick_j/src/models"
	"gorm.io/gorm"
)

func CreateUser(tx *gorm.DB, user models.User) (uint, error) {
	res := tx.Create(&user).Error
	return user.ID, res
}

func AuthUser(userName, password string) (models.User, error) {
	user := models.User{}
	res := configs.DB.Where("user_name=? AND  password=?", userName, password).First(&user)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		return models.User{}, res.Error
	}
	return user, nil
}

func EditUserByDistId(userId string, userIn dto.EditUserIn) (dto.EditUserIn, *gorm.DB) {

	result := configs.DB.Model(models.User{}).Where("id=?", userId).Updates(userIn)
	return userIn, result
}

func GetUserByEmail(email string) (models.User, *gorm.DB) {
	var user models.User
	err := configs.DB.Where("email = ?", email).First(&user)

	return user, err
}
