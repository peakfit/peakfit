package repositories

import (
	"github.com/peak-fit/krithick_j/src/models"
	"gorm.io/gorm"
)

func CreateMuscle(tx *gorm.DB, muscleObj models.Muscle) error {
	res := tx.Create(&muscleObj)
	if res.Error != nil {
		return res.Error
	}
	return nil
}
