package routes

import (
	"github.com/gofiber/fiber/v2"
	"github.com/peak-fit/krithick_j/src/controllers"
)

func MuscleRouter(router fiber.Router) {
	router.Post("/new", controllers.CreateMuscle)
}
