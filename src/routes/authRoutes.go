package routes

import (
	"github.com/gofiber/fiber/v2"
	"github.com/peak-fit/krithick_j/src/controllers"
)

func AuthRouter(router fiber.Router) {
	router.Post("/register", controllers.UserRegistration)
	router.Post("/login", controllers.Login)
}
