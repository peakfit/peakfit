package service

import (
	"html/template"
	"os"

	"github.com/jenazads/gomail"
)

type EmailData struct {
	Name           string
	WelcomeMessage string
}

func SendWelcomeEmail(toMail string, name string) error {

	// Parse the email template
	tmpl, err := template.ParseFiles("assets/templates/welcome_email.html")
	if err != nil {
		return err
	}

	// Create a new file to store the rendered email content
	file, err := os.Create("email.html")
	if err != nil {
		return err
	}
	defer file.Close()

	// Execute the template with the coupons data and write it to the file
	data := EmailData{Name: name, WelcomeMessage: "Let your Fitness be at Peak."}
	err = tmpl.Execute(file, data)
	if err != nil {
		return err
	}

	// Read the contents of the rendered HTML file
	renderedEmail, err := os.ReadFile("email.html")
	if err != nil {
		return err
	}

	m, err := gomail.NewGoMail()
	if err != nil {
		return err
	}

	m.Set("Username", "j.krithick@gmail.com")
	m.Set("Password", "gior zeiv xgga lqky")

	m.Set("Servername", "smtp.gmail.com:465")

	m.Set("From", "j.krithick@gmail.com")
	m.Set("From_name", "Peak.Fit")

	m.Set("To", toMail)

	m.Set("Subject", "Welcome to Peak.Fit")

	m.Set("BodyMessage", string(renderedEmail))

	if err := m.SendMessage(); err != nil {
		return err
	}
	return nil
}

// func SendWelcomeEmail(toMail string, name string) error {

// 	msg := fmt.Sprintf("Welcome %s to Peak.Fit! Let your Fitness be at Peak.", name)
// 	tomail := fmt.Sprintf("<%s>", toMail)
// 	SendMail(tomail, "Welcome", msg)
// 	return nil
// }
