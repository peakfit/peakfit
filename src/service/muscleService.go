package service

import (
	"github.com/gofiber/fiber/v2"
	"github.com/peak-fit/krithick_j/configs"
	"github.com/peak-fit/krithick_j/src/models"
	"github.com/peak-fit/krithick_j/src/repositories"
)

func CreateMuscle(muscleType string) (fiber.Map, int) {

	muscleObj := models.Muscle{
		Type: muscleType,
	}
	//Succeed all or fail all
	tx := configs.DB.Begin()
	res := repositories.CreateMuscle(tx, muscleObj)

	if res != nil {
		tx.Rollback()
		return fiber.Map{"error": res.Error()}, fiber.StatusInternalServerError
	}

	if err := tx.Commit().Error; err != nil {
		tx.Rollback()
		return fiber.Map{"Error": err.Error()}, fiber.StatusInternalServerError

	}
	return fiber.Map{"data": "Account Successfully created :)"}, fiber.StatusCreated
}
