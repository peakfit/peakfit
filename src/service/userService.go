package service

import (
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/log"
	"github.com/peak-fit/krithick_j/src/dto"
	"github.com/peak-fit/krithick_j/src/repositories"
)

func EditUserByUserId(userId string, userIn dto.EditUserIn) (fiber.Map, int) {

	user, result := repositories.EditUserByDistId(userId, userIn)

	if result.Error != nil {
		log.Info("Error saving user to the database:", result.Error)
		return fiber.Map{"error": result.Error}, http.StatusBadGateway
	}

	if result.Error != nil {
		return fiber.Map{"error": result.Error}, http.StatusInternalServerError
	}
	return fiber.Map{"success": "User Updated Successfully", "Deleted_User": user}, http.StatusOK
}
