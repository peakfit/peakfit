package service

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt"
	"github.com/peak-fit/krithick_j/configs"
	"github.com/peak-fit/krithick_j/src/dto"
	"github.com/peak-fit/krithick_j/src/models"
	"github.com/peak-fit/krithick_j/src/repositories"
)

func RegisterUser(user_in dto.UserIn) (fiber.Map, error) {

	user := models.User{
		UserName: user_in.UserName,
		Password: fmt.Sprintf("%x", sha256.Sum256([]byte(user_in.Password))),
		Email:    user_in.Email,
	}

	// Check if the email already exists
	existingUser, _ := repositories.GetUserByEmail(user_in.Email)

	if existingUser.Email == user_in.Email {
		return fiber.Map{"error": "Email already exists"}, errors.New("email already exists")
	}

	//Succeed all or fail all
	tx := configs.DB.Begin()
	user_id, _ := repositories.CreateUser(tx, user)

	if err := tx.Commit().Error; err != nil {
		tx.Rollback()
		return fiber.Map{"Error": err.Error()}, err

	}

	//send welcome email
	err1 := SendWelcomeEmail(user_in.Email, user_in.UserName)
	if err1 != nil {
		return fiber.Map{"Error": err1.Error}, err1
	}

	return fiber.Map{"user_id": user_id}, nil
}

func LoginUser(userName string, password string) (fiber.Map, int) {

	pass := fmt.Sprintf("%x", sha256.Sum256([]byte(password)))

	res, err := repositories.AuthUser(userName, pass)
	if err != nil {
		return fiber.Map{"error": "Username or password is incorrect"}, fiber.StatusUnauthorized
	}

	if res.UserName != userName || res.Password != pass {
		return fiber.Map{"error": "No user with this username exist!"}, fiber.StatusUnauthorized
	}

	claims := jwt.MapClaims{
		"name":  res.UserName,
		"admin": false,
		"exp":   time.Now().AddDate(0, 6, 0).Unix(),
	}

	// Create token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Generate encoded token and send it as response.
	tokenstring, err := token.SignedString([]byte("secret"))
	if err != nil {
		return fiber.Map{"err": err.Error()}, fiber.StatusInternalServerError
	}
	authout := dto.AuthOut{
		UserDetails: res,
		AuthToken:   tokenstring,
	}
	return fiber.Map{"data": authout}, fiber.StatusAccepted
}
