package controllers

import (
	"github.com/gofiber/fiber/v2"
	"github.com/peak-fit/krithick_j/src/dto"
	"github.com/peak-fit/krithick_j/src/service"
)

func EditUserByUserId(c *fiber.Ctx) error {

	userID := c.Params("user_id")

	var user_in dto.EditUserIn

	if err := c.BodyParser(&user_in); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"status": "fail", "message": err.Error()})
	}

	res, status := service.EditUserByUserId(userID, user_in)

	return c.Status(status).JSON(res)
}
