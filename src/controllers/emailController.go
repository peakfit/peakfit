package controllers

import (
	"github.com/gofiber/fiber/v2"
	"github.com/peak-fit/krithick_j/src/service"
)

func SendWelcomeEmail(c *fiber.Ctx) error {

	data := struct {
		Email string `json:"email"`
		Name  string `json:"name"`
	}{}
	err := c.BodyParser(&data)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).Send([]byte(err.Error()))
	}
	service.SendWelcomeEmail(data.Email, data.Name)
	c.Status(fiber.StatusCreated).SendString("Created")
	return nil
}
