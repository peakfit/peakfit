package controllers

import (
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/peak-fit/krithick_j/src/dto"
	"github.com/peak-fit/krithick_j/src/service"
)

func UserRegistration(c *fiber.Ctx) error {
	user_in := dto.UserIn{}
	if err := c.BodyParser(&user_in); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"status": "fail", "message": err.Error()})
	}

	resp, err := service.RegisterUser(user_in)
	if err != nil {
		return c.Status(http.StatusBadRequest).JSON(resp)
	} else {
		return c.Status(http.StatusCreated).JSON(resp)
	}
}

func Login(c *fiber.Ctx) error {
	data := struct {
		UserName string `json:"user_name"`
		Password string `json:"password"`
	}{}

	if err := c.BodyParser(&data); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"status": "fail", "message": err.Error()})
	}

	res, status := service.LoginUser(data.UserName, data.Password)
	return c.Status(status).JSON(res)
}
