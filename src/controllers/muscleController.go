package controllers

import (
	"github.com/gofiber/fiber/v2"
	"github.com/peak-fit/krithick_j/src/service"
)

func CreateMuscle(c *fiber.Ctx) error {

	var data struct {
		MuscleType string `json:"muscle_type"`
	}

	if err := c.BodyParser(&data); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"status": "fail", "message": err.Error()})
	}

	res, status := service.CreateMuscle(data.MuscleType)

	return c.Status(status).JSON(res)
}
