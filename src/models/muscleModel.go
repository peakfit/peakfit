package models

import "gorm.io/gorm"

type Muscle struct {
	gorm.Model
	Type string
}