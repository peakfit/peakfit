package models

import "gorm.io/gorm"

type User struct {
	gorm.Model
	UserName       string
	Password       string
	Gender         string
	Age            int
	Height         float64
	Weight         float64
	DisplayProfile string
	PhysicalLevel  string
	Email          string
}
