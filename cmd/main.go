package main

import (
	"fmt"
	"log"

	jwtware "github.com/gofiber/contrib/jwt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	_ "github.com/golang-jwt/jwt/v5"
	"github.com/peak-fit/krithick_j/configs"
	"github.com/peak-fit/krithick_j/src/controllers"
	"github.com/peak-fit/krithick_j/src/routes"
)

func init() {
	fmt.Println("Initializing the DB")
	config, err := configs.LoadConfig(".")
	if err != nil {
		log.Fatalln("\x1b[31mFailed to load environment variables!\x1b[0m \n", err.Error())
	}
	configs.DbConnect(config)
}

func main() {
	app := fiber.New()
	app.Use(recover.New())
	app.Use(cors.New())
	app.Use(logger.New())
	// app.Get("/media/:filename", controllers.GetMediaFile)	
	api := app.Group("/api")
	api.Route("/auth", routes.AuthRouter)
	api.Put("updateUser/:user_id", controllers.EditUserByUserId)
	api.Post("/welcomeemail", controllers.SendWelcomeEmail)

	app.Use(jwtware.New(jwtware.Config{
		SigningKey: jwtware.SigningKey{Key: []byte("secret")},
	}))

	/* Hereafter all the endpoints will be secured */
	api.Route("/user", routes.UserRouter)
	api.Route("/muscle", routes.MuscleRouter)

	app.Listen(fmt.Sprintf(":%d", configs.GlobalConfig.AppPort))
}
